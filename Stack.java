public class Stack {

    int maxSize;
    char[] stack;
    int top;

    public Stack(int size) {
        maxSize=size;
        stack=new char[maxSize];
        top=-1;
    }

    public void push(char d) {
        stack[++top]=d;
    }
    public char pop(){
        return stack[top--];
    }
    public boolean isEmpty(){
        return (top==-1);
    }
    public void clear(){
        while (top >-1)
            pop();
    }
    public void display(){
        for (int i=0;i<= top;i++)
            System.out.println(stack[i]+" ");
        System.out.println();
    }
}