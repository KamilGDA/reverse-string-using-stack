import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DoRev {
    public static void main(String[] args) throws IOException {

        System.out.println("Enter String");
        InputStreamReader ir=new InputStreamReader(System.in);
        BufferedReader br=new BufferedReader(ir);
        String inpt=br.readLine();

        StringReverse str=new StringReverse(inpt);
        str.insertStack();
        str.doRev();
    }
}