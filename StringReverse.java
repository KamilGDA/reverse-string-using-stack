public class StringReverse {
    String input;
    String output;
    Stack stk;

    public StringReverse(String in) {
        input=in;
    }
    public void insertStack(){
        stk=new Stack(input.length());
        for (int i=0; i<input.length();i++)
            stk.push(input.charAt(i));
    }
    public void doRev(){
        output="";
        while (!stk.isEmpty())
            output+=stk.pop();
        System.out.println(output);
    }
}